import java.util.Scanner;

public class BlockDiagram {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter a: ");
        int a = in.nextInt();

        System.out.println("Enter b: ");
        int b = in.nextInt();
        int c;

        if (a <= b) {
            c = a + b;
        }
        else {
            c = a - b;
        }
        System.out.println(c);
    }
}
